import pandas as pd
import scipy as sp
from sklearn.tree import DecisionTreeRegressor

# Path of the file to read
iowa_file_path = 'Dataset/kc_house_data.csv'

train_data = pd.read_csv(iowa_file_path, parse_dates=['date'])
train_data.columns

# selecting my prediction target
y = train_data.price
train_data_features=['id','bedrooms','bathrooms','lat','long']
X = train_data[train_data_features]
# Call line below with no argument to check that you've loaded the data correctly
# print(X.describe())

my_model= DecisionTreeRegressor(random_state =1)
my_model.fit(X, y)
 
#using the selected features above to predict the prices.
my_prediction = my_model.predict(X)

Id = train_data ['id']


my_submission = pd.DataFrame({'id':Id, 'price': my_prediction})
my_submission.to_csv('DTsubmission.csv', index= False)

print(my_submission)
