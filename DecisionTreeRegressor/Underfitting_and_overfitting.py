import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error

# Path of the file to read
iowa_file_path = 'Dataset/kc_house_data.csv'

home_data = pd.read_csv(iowa_file_path, parse_dates=['date'])

# selecting my prediction target
y = home_data.price
home_data_features=['id','bedrooms','bathrooms','lat','long']
X = home_data[home_data_features]
# Call line below with no argument to check that you've loaded the data correctly
# print(X.describe())

#ModelValidation, here I'm splitting my X and y into training set and a validation set respectively
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state = 1)

def get_mae(max_leaf_nodes, train_X, val_X, train_y, val_y):
    model = DecisionTreeRegressor(max_leaf_nodes = max_leaf_nodes, random_state=0)
    model.fit(train_X, train_y)
    preds_val = model.predict(val_X)
    mae = mean_absolute_error(val_y, preds_val)
    return(mae)

candidate_max_leaf_nodes = [5, 25, 50, 100, 250]
# A loop to find the ideal tree size from candidate_max_leaf_nodes
my_Loop = {ideal_tree_size: get_mae(ideal_tree_size, train_X, val_X, train_y, val_y) for ideal_tree_size in candidate_max_leaf_nodes }
# Store the best value of max_leaf_nodes (it will be either 5, 25, 50, 100, 250 or 500)
best_tree_size = min(my_Loop, key=my_Loop.get)

print(best_tree_size)