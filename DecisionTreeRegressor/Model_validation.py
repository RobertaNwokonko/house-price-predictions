import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error

# Path of the file to read
iowa_file_path = 'Dataset/kc_house_data.csv'

home_data = pd.read_csv(iowa_file_path, parse_dates=['date'])

# selecting my prediction target
y = home_data.price
home_data_features=['id','bedrooms','bathrooms','lat','long']
X = home_data[home_data_features]
# Call line below with no argument to check that you've loaded the data correctly
# print(X.describe())

#ModelValidation, here I'm splitting my X and y into training set and a validation set respectively
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state = 1)

#this is just to get a clue of what the split looks like
#print(train_X.describe())

# Specify the model
my_model= DecisionTreeRegressor(random_state= 1)

# Fit model with the training data.
my_model.fit(train_X, train_y)
check_input = True

# predicting using my validation set
val_prediction= my_model.predict(val_X)

#Finding the mean absolute error between val_y(this is just a split set from y) and newly predicted values 
Error_found = mean_absolute_error(val_y, val_prediction)

# for underfitting I learnt how to write an explicit loop to choose the best tree size from a list of tree sizes 

print (Error_found)

