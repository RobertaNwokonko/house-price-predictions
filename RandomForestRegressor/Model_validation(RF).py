from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
import pandas as pd

iowa_file_path = 'Dataset/kc_house_data.csv'

home_data = pd.read_csv(iowa_file_path)

features = ["id","bedrooms","sqft_living15", "sqft_lot15","lat"]

X = home_data[features]

#specifying my prediction target
y = home_data.price

#split dataset into training and validation set
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state =2)


model = RandomForestRegressor(random_state= 2)
model.fit(train_X, train_y)

#using the validation set to predict the prices here: Remember that it was the training set that was used to train(or fit) the model
prediction = model.predict(val_X)

new_mae = mean_absolute_error(prediction, val_y)

#to check the difference error using RandomForest vs DecisionTree
print (new_mae)


