import pandas as pd
from sklearn.ensemble import RandomForestRegressor

iowa_file_path = "Dataset/kc_house_data.csv"
train_data = pd.read_csv (iowa_file_path)

train_data.columns
features = ["id","bedrooms","sqft_living15", "sqft_lot15","lat"]

#prediction features
X = train_data[features]

# selecting my prediction target
y = train_data.price

model=RandomForestRegressor(random_state=2)

model.fit(X, y)
prediction= model.predict(X)

Id = train_data ['id']

my_submission = pd.DataFrame({'id':Id, 'price': prediction})
my_submission.to_csv('RFsubmission.csv', index=False)

print(my_submission)

