import pandas as pd
from sklearn.model_selection import train_test_split


iowa_data = 'Dataset/kc_house_data.csv'

home_data= pd.read_csv(iowa_data)

# make copy to avoid changing original data (incase I might have to Impute)
new_data = home_data.copy()

#to find which columns have null values
missing_column = (new_data.isnull().sum())
print(missing_column)

#from the result displayed, there are no missing columns 